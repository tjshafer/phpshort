<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::drop('cronjobs');

        DB::table('settings')->insert(
            [
                ['name' => 'cronjob_executed_at', 'value' => null],
            ]
        );
    }
};
