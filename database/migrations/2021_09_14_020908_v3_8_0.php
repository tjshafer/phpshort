<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::table('settings')->insert(
            [
                [
                    'name' => 'short_gsb',
                    'value' => '0',
                ],
                [
                    'name' => 'short_gsb_key',
                    'value' => '',
                ],
            ]
        );
    }
};
