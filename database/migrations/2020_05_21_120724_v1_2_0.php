<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('users', function ($table) {
            $table->smallInteger('default_domain')->after('role')->nullable();
            $table->smallInteger('default_space')->after('default_domain')->nullable();
            $table->smallInteger('default_stats')->after('default_space')->nullable();
        });
    }
};
