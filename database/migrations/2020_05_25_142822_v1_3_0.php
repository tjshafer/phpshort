<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('plans', function (Blueprint $table) {
            $table->text('coupons')->after('amount_year')->nullable();
        });

        Schema::table('links', function (Blueprint $table) {
            $table->integer('last_rotation')->change();
        });
    }
};
