<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->integer('id', true);
            $table->string('title', 255)->index('title');
            $table->string('slug', 255);
            $table->tinyInteger('footer');
            $table->text('content');
            $table->timestamps();
        });
    }
};
