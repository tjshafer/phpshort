<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::table('links')->where(['privacy' => 0])->update(['privacy' => 3]);
        DB::table('links')->where(['privacy' => 1])->update(['privacy' => 0]);
        DB::table('links')->where(['privacy' => 3])->update(['privacy' => 1]);
    }
};
