<?php

return [

    'software' => [
        'name' => 'phpShort',
        'author' => 'Lunatio',
        'url' => 'https://lunatio.com',
        'version' => '4.8.0',
    ],

];
