<?php

/**
 * Calculate the growth between two values.
 *
 * @param $current
 * @param $previous
 */
function calcGrowth($current, $previous): array|int
{
    if ($previous == 0 || $previous == null || $current == 0) {
        return 0;
    }

    return $result = (($current - $previous) / $previous * 100);
}
