<?php

/**
 * Get the local host IP.
 */
function getHostIp(): ?string
{
    $hostName = str_replace(['http://', 'https://'], '', config('app.url'));

    $hostIp = gethostbyname($hostName);

    // Check if the host IP is not empty & returns an actual IP address
    if ($hostIp != $hostName) {
        return $hostIp;
    }

    return request()->server('SERVER_ADDR');
}

/**
 * Get the remote host IP.
 *
 * @param $hostName
 */
function getRemoteIp($hostName): string
{
    $remoteIp = gethostbyname($hostName);

    // Check if the host IP is not empty & returns an actual IP address
    if ($remoteIp != $hostName) {
        return $remoteIp;
    }

    return false;
}
