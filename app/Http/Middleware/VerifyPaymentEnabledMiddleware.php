<?php

namespace App\Http\Middleware;

use Closure;

class VerifyPaymentEnabledMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @return mixed
     */
    public function handle(\Illuminate\Http\Request $request, Closure $next)
    {
        // Check if any payment processor is enabled
        if (! paymentProcessors()) {
            return to_route('home');
        }

        return $next($request);
    }
}
