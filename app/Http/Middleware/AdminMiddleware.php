<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

class AdminMiddleware
{
    /**
     * AdminMiddleware constructor.
     */
    public function __construct(
        /**
         * @var
         */
        protected Guard $auth
    ) {
    }

    /**
     * Handle an incoming request.
     *
     * @return mixed
     */
    public function handle(\Illuminate\Http\Request $request, Closure $next)
    {
        // If the user is a guest, or doesn't have permissions
        if ($this->auth->guest() || $this->auth->user()->role != 1) {
            return to_route('home');
        }

        return $next($request);
    }
}
