<?php

namespace App\Http\Middleware;

use Closure;

class APIGuardMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @return mixed
     */
    public function handle(\Illuminate\Http\Request $request, Closure $next)
    {
        if ($request->user()->cannot('api', [\App\Models\Link::class, $request->user()->plan->features->api])) {
            return response()->json([
                'message' => __('You don\'t have access to this feature.'),
                'status' => 403,
            ], 403);
        }

        return $next($request);
    }
}
