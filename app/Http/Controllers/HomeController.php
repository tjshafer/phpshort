<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreLinkRequest;
use App\Models\Domain;
use App\Models\Link;
use App\Models\Plan;
use App\Traits\LinkTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    use LinkTrait;

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable|\Illuminate\Http\RedirectResponse
     */
    public function index(Request $request): \Illuminate\Http\RedirectResponse|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        // If the user is logged-in, redirect to dashboard
        if (Auth::check()) {
            return to_route('dashboard');
        }

        // Get the local host
        $local = parse_url(config('app.url'));

        // Get the request host
        $remote = $request->getHost();

        if ($local['host'] != $remote) {
            // Get the remote domain
            $domain = Domain::where('name', '=', $remote)->first();

            // If the domain exists
            if ($domain) {
                // If the domain has an index page defined
                if ($domain->index_page) {
                    return redirect()->to($domain->index_page, 301)->header('Cache-Control', 'no-store, no-cache, must-revalidate');
                }

                return redirect()->to(config('app.url'), 301)->header('Cache-Control', 'no-store, no-cache, must-revalidate');
            }
        }

        // If there's a custom site index
        if (config('settings.index')) {
            return redirect()->to(config('settings.index'), 301)->header('Cache-Control', 'no-store, no-cache, must-revalidate');
        }

        // If there's a payment processor enabled
        if (paymentProcessors()) {
            $user = Auth::user();

            $plans = Plan::where('visibility', 1)->oldest('position')->oldest('id')->get();

            $domains = Domain::select('name')->where('user_id', '=', 0)
                ->whereNotIn('id', [config('settings.short_domain')])
                ->get()
                ->map(fn ($item) => $item->name)
                ->toArray();
        } else {
            $user = null;
            $plans = null;
            $domains = null;
        }

        $defaultDomain = null;

        if (Domain::where([['user_id', '=', 0], ['id', '=', config('settings.short_domain')]])->exists()) {
            $defaultDomain = config('settings.short_domain');
        }

        return view('home.index', ['plans' => $plans, 'user' => $user, 'domains' => $domains, 'defaultDomain' => $defaultDomain]);
    }

    /**
     * Store the Link.
     *
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function createLink(StoreLinkRequest $request): \Illuminate\Http\RedirectResponse
    {
        abort_if(! config('settings.short_guest'), 404);

        $this->linkStore($request);

        return redirect()->back()->with('link', Link::where('user_id', '=', 0)->latest('id')->limit(1)->get());
    }
}
