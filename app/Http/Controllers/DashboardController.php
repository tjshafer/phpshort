<?php

namespace App\Http\Controllers;

use App\Models\Link;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    /**
     * Show the Dashboard page.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index(Request $request): \Illuminate\Http\RedirectResponse|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        // If the user previously selected a plan
        if (! empty($request->session()->get('plan_redirect'))) {
            return to_route('checkout.index', ['id' => $request->session()->get('plan_redirect')['id'], 'interval' => $request->session()->get('plan_redirect')['interval']]);
        }

        $latestLinks = Link::with('domain')->where('user_id', $request->user()->id)->latest('id')->limit(5)->get();

        $clicks = [];

        $popularLinks = Link::with('domain')->where('user_id', $request->user()->id)->latest('clicks')->limit(5)->get();

        return view('dashboard.index', ['user' => $request->user(), 'latestLinks' => $latestLinks, 'clicks' => $clicks, 'popularLinks' => $popularLinks]);
    }
}
