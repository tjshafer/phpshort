<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreDomainRequest;
use App\Http\Requests\UpdateDomainRequest;
use App\Models\Domain;
use App\Traits\DomainTrait;
use Illuminate\Http\Request;

class DomainController extends Controller
{
    use DomainTrait;

    /**
     * List the Domains.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $search = $request->input('search');
        $searchBy = in_array($request->input('search_by'), ['name']) ? $request->input('search_by') : 'name';
        $sortBy = in_array($request->input('sort_by'), ['id', 'name']) ? $request->input('sort_by') : 'id';
        $sort = in_array($request->input('sort'), ['asc', 'desc']) ? $request->input('sort') : 'desc';
        $perPage = in_array($request->input('per_page'), [10, 25, 50, 100]) ? $request->input('per_page') : config('settings.paginate');

        $domains = Domain::where('user_id', $request->user()->id)
            ->when($search, fn ($query) => $query->searchName($search))
            ->orderBy($sortBy, $sort)
            ->paginate($perPage)
            ->appends(['search' => $search, 'search_by' => $searchBy, 'sort_by' => $sortBy, 'sort' => $sort, 'per_page' => $perPage]);

        return view('domains.container', ['view' => 'list', 'domains' => $domains]);
    }

    /**
     * Show the create Domain form.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        return view('domains.container', ['view' => 'new']);
    }

    /**
     * Show the edit Domain form.
     *
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Request $request, $id): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $domain = Domain::where([['id', '=', $id], ['user_id', '=', $request->user()->id]])->firstOrFail();

        return view('domains.container', ['view' => 'edit', 'domain' => $domain]);
    }

    /**
     * Store the Domain.
     */
    public function store(StoreDomainRequest $request): \Illuminate\Http\RedirectResponse
    {
        $this->domainStore($request);

        return to_route('domains')->with('success', __(':name has been created.', ['name' => $request->input('name')]));
    }

    /**
     * Update the Domain.
     *
     * @param $id
     */
    public function update(UpdateDomainRequest $request, $id): \Illuminate\Http\RedirectResponse
    {
        $domain = Domain::where([['id', '=', $id], ['user_id', '=', $request->user()->id]])->firstOrFail();

        $this->domainUpdate($request, $domain);

        return redirect()->back()->with('success', __('Settings saved.'));
    }

    /**
     * Delete the Domain.
     *
     * @param $id
     *
     * @throws \Exception
     */
    public function destroy(Request $request, $id): \Illuminate\Http\RedirectResponse
    {
        $domain = Domain::where([['id', '=', $id], ['user_id', '=', $request->user()->id]])->firstOrFail();

        $domain->delete();

        return to_route('domains')->with('success', __(':name has been deleted.', ['name' => $domain->name]));
    }
}
