<?php

namespace App\Http\View\Composers;

use App\Models\Domain;
use App\Models\Link;
use App\Models\Pixel;
use App\Models\Space;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;

class UserStatsComposer
{
    /**
     * @var
     */
    public $stats;

    /**
     * Bind data to the view.
     */
    public function compose(View $view): void
    {
        if (Auth::check()) {
            $user = Auth::user();

            if (! $this->stats) {
                $this->stats = [
                    'links' => Link::where('user_id', $user->id)->count(),
                    'spaces' => Space::where('user_id', $user->id)->count(),
                    'domains' => Domain::where('user_id', $user->id)->count(),
                    'pixels' => Pixel::where('user_id', $user->id)->count(),
                ];
            }

            $view->with('stats', $this->stats);
        }
    }
}
