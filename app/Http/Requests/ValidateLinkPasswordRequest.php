<?php

namespace App\Http\Requests;

use App\Rules\ValidateLinkPasswordRule;
use Illuminate\Foundation\Http\FormRequest;

class ValidateLinkPasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'password' => ['required', new ValidateLinkPasswordRule($this->route('id'))],
        ];
    }
}
