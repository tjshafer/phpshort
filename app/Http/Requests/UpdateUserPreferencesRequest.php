<?php

namespace App\Http\Requests;

use App\Rules\ValidateDomainOwnershipRule;
use App\Rules\ValidateSpaceOwnershipRule;
use Illuminate\Foundation\Http\FormRequest;

class UpdateUserPreferencesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'default_space' => ['nullable', 'integer', new ValidateSpaceOwnershipRule($this->user()->id)],
            'default_domain' => ['integer', new ValidateDomainOwnershipRule($this->user())],
            'default_stats' => ['nullable', 'integer', 'between:0,1'],
        ];
    }
}
