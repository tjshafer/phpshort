<?php

namespace App\Policies;

use App\Models\Pixel;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PixelPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any pixels.
     *
     * @return mixed
     */
    public function viewAny(User $user): void
    {
        //
    }

    /**
     * Determine whether the user can view the pixel.
     *
     * @return mixed
     */
    public function view(User $user, Pixel $pixel): void
    {
        //
    }

    /**
     * Determine whether the user can create pixels.
     *
     * @return mixed
     */
    public function create(User $user, $limit): bool
    {
        if ($limit == -1) {
            return true;
        }
        if ($limit > 0) {
            $count = Pixel::where('user_id', '=', $user->id)->count();
            if ($count < $limit) {
                return true;
            }
        }

        return false;
    }

    /**
     * Determine whether the user can update the pixel.
     *
     * @return mixed
     */
    public function update(User $user, Pixel $pixel): void
    {
        //
    }

    /**
     * Determine whether the user can delete the pixel.
     *
     * @return mixed
     */
    public function delete(User $user, Pixel $pixel): void
    {
        //
    }

    /**
     * Determine whether the user can restore the pixel.
     *
     * @return mixed
     */
    public function restore(User $user, Pixel $pixel): void
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the pixel.
     *
     * @return mixed
     */
    public function forceDelete(User $user, Pixel $pixel): void
    {
        //
    }
}
