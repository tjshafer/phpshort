<?php

namespace App\Policies;

use App\Models\Space;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class SpacePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any spaces.
     *
     * @return mixed
     */
    public function viewAny(User $user): void
    {
        //
    }

    /**
     * Determine whether the user can view the space.
     *
     * @return mixed
     */
    public function view(User $user, Space $space): void
    {
        //
    }

    /**
     * Determine whether the user can create spaces.
     *
     * @return mixed
     */
    public function create(User $user, $limit): bool
    {
        if ($limit == -1) {
            return true;
        }
        if ($limit > 0) {
            $count = Space::where('user_id', '=', $user->id)->count();
            if ($count < $limit) {
                return true;
            }
        }

        return false;
    }

    /**
     * Determine whether the user can update the space.
     *
     * @return mixed
     */
    public function update(User $user, Space $space): void
    {
        //
    }

    /**
     * Determine whether the user can delete the space.
     *
     * @return mixed
     */
    public function delete(User $user, Space $space): void
    {
        //
    }

    /**
     * Determine whether the user can restore the space.
     *
     * @return mixed
     */
    public function restore(User $user, Space $space): void
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the space.
     *
     * @return mixed
     */
    public function forceDelete(User $user, Space $space): void
    {
        //
    }
}
