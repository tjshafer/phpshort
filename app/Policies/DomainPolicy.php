<?php

namespace App\Policies;

use App\Models\Domain;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class DomainPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any domains.
     *
     * @return mixed
     */
    public function viewAny(User $user): void
    {
        //
    }

    /**
     * Determine whether the user can view the domain.
     *
     * @return mixed
     */
    public function view(User $user, Domain $domain): void
    {
        //
    }

    /**
     * Determine whether the user can create domains.
     *
     * @return mixed
     */
    public function create(User $user, $limit): bool
    {
        if ($limit == -1) {
            return true;
        }
        if ($limit > 0) {
            $count = Domain::where('user_id', '=', $user->id)->count();
            if ($count < $limit) {
                return true;
            }
        }

        return false;
    }

    /**
     * Determine whether the user can update the domain.
     *
     * @return mixed
     */
    public function update(User $user, Domain $domain): void
    {
        //
    }

    /**
     * Determine whether the user can delete the domain.
     *
     * @return mixed
     */
    public function delete(User $user, Domain $domain): void
    {
        //
    }

    /**
     * Determine whether the user can restore the domain.
     *
     * @return mixed
     */
    public function restore(User $user, Domain $domain): void
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the domain.
     *
     * @return mixed
     */
    public function forceDelete(User $user, Domain $domain): void
    {
        //
    }
}
