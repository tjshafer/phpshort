<?php

namespace App\Policies;

use App\Models\Link;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class LinkPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any links.
     *
     * @return mixed
     */
    public function viewAny(User $user): void
    {
        //
    }

    /**
     * Determine whether the user can view the link.
     *
     * @return mixed
     */
    public function view(User $user, Link $link): void
    {
        //
    }

    /**
     * Determine whether the user can create links.
     *
     * @param $limit
     * @return mixed
     */
    public function create(User $user, $limit): bool
    {
        if ($limit == -1) {
            return true;
        }
        if ($limit > 0) {
            // Set the count for multi links counter
            $mCount = 0;
            // If the request is for a multi links creation
            if (request()->input('multiple_links')) {
                // Get the links
                $links = preg_split('/\n|\r/', request()->input('urls'), -1, PREG_SPLIT_NO_EMPTY);

                // If the request contains more than one link
                if (count(preg_split('/\n|\r/', request()->input('urls'), -1, PREG_SPLIT_NO_EMPTY)) > 1) {
                    // Get the links count, and subtract 1 value, the remaining will be used to emulate the total links count against the limit
                    $mCount = (count($links) - 1);
                }
            }
            $count = Link::where('user_id', '=', $user->id)->count();
            // If the total links count (including multi links, if any in the request) exceeds the limits
            if (($count + $mCount) < $limit) {
                return true;
            }
        }

        return false;
    }

    /**
     * Determine whether the user can update the link.
     *
     * @return mixed
     */
    public function update(User $user, Link $link): void
    {
        //
    }

    /**
     * Determine whether the user can delete the link.
     *
     * @return mixed
     */
    public function delete(User $user, Link $link): void
    {
        //
    }

    /**
     * Determine whether the user can restore the link.
     *
     * @return mixed
     */
    public function restore(User $user, Link $link): void
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the link.
     *
     * @return mixed
     */
    public function forceDelete(User $user, Link $link): void
    {
        //
    }

    /**
     * Determine whether the user can use Spaces.
     *
     * @param $limit
     */
    public function spaces(User $user, $limit): bool
    {
        if ($limit) {
            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can use Domains.
     *
     * @param $limit
     */
    public function domains(User $user, $limit): bool
    {
        if ($limit) {
            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can use Pixels.
     *
     * @param $limit
     */
    public function pixels(User $user, $limit): bool
    {
        if ($limit) {
            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can use Stats.
     *
     * @param $limit
     */
    public function stats(User $user, $limit): bool
    {
        if ($limit) {
            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can Disable links.
     *
     * @param $limit
     */
    public function disabled(User $user, $limit): bool
    {
        if ($limit) {
            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can use Targeting.
     *
     * @param $limit
     */
    public function targeting(User $user, $limit): bool
    {
        if ($limit) {
            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can use UTM.
     *
     * @param $limit
     */
    public function utm(User $user, $limit): bool
    {
        if ($limit) {
            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can use Password.
     *
     * @param $limit
     */
    public function password(User $user, $limit): bool
    {
        if ($limit) {
            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can use Expire.
     *
     * @param $limit
     */
    public function expiration(User $user, $limit): bool
    {
        if ($limit) {
            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can use Global Domains.
     *
     * @param $limit
     */
    public function globalDomains(User $user, $limit): bool
    {
        if ($limit) {
            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can use Deep Links.
     *
     * @param $limit
     */
    public function deepLinks(User $user, $limit): bool
    {
        if ($limit) {
            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can use Data Export.
     *
     * @param $limit
     */
    public function dataExport(?User $user, $limit): bool
    {
        if ($limit) {
            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can use the API.
     *
     * @param $limit
     */
    public function api(User $user, $limit): bool
    {
        if ($limit) {
            return true;
        }

        return false;
    }
}
