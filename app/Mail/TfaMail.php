<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class TfaMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @param $code
     */
    public function __construct(
        /**
         * @var
         */
        public $code
    ) {
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): static
    {
        $this->subject(formatTitle([__('Security code'), config('settings.title')]));

        return $this->markdown('emails.tfa', [
            'message' => __('Your security code is: :code.', ['code' => $this->code]),
        ]);
    }
}
