<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Crypt;

/**
 * Class Link
 *
 * @mixin Builder
 */
class Link extends Model
{
    protected $casts = [
        'ends_at' => 'datetime',
        'country_target' => 'object',
        'platform_target' => 'object',
        'language_target' => 'object',
        'rotation_target' => 'object',
    ];

    protected function totalClicks(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $this->hasMany(\App\Models\Stat::class)->where('link_id', $this->id)->count(),
        );
    }

    /**
     * Get the Space of the Link.
     */
    public function space(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(\App\Models\Space::class, 'id', 'space_id');
    }

    /**
     * Get the Domain of the Link.
     */
    public function domain(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(\App\Models\Domain::class, 'id', 'domain_id');
    }

    /**
     * Get the Stats of the Link.
     */
    public function stats(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(\App\Models\Stat::class)->where('link_id', $this->id);
    }

    /**
     * Get the user that owns the Link.
     *
     * @return mixed
     */
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class)->withTrashed();
    }

    /**
     * Get the Pixels of the Link.
     */
    public function pixels(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(\App\Models\Pixel::class);
    }

    /**
     * @param $value
     */
    public function scopeSearchTitle(Builder $query, $value): Builder
    {
        return $query->where('title', 'like', '%'.$value.'%');
    }

    /**
     * @param $value
     */
    public function scopeSearchUrl(Builder $query, $value): Builder
    {
        return $query->where('url', 'like', '%'.$value.'%');
    }

    /**
     * @param $value
     */
    public function scopeSearchAlias(Builder $query, $value): Builder
    {
        return $query->where('alias', 'like', '%'.$value.'%');
    }

    /**
     * @param $value
     */
    public function scopeOfUser(Builder $query, $value): Builder
    {
        return $query->where('user_id', '=', $value);
    }

    /**
     * @param $value
     * @return Builder
     */
    public function scopeOfSpace(Builder $query, $value)
    {
        return $query->where('space_id', '=', $value)
            ->when(! $value, function ($query) {
                $query->orWhereNull('space_id');
            });
    }

    /**
     * @param $value
     */
    public function scopeOfDomain(Builder $query, $value): Builder
    {
        return $query->where('domain_id', '=', $value);
    }

    public function scopeExpired(Builder $query): Builder
    {
        return $query->where(function ($query) {
            $query->whereNotNull('ends_at')
                ->where('ends_at', '<', Carbon::now());
        })->orWhere(function ($query) {
            $query->where('expiration_clicks', '>', 0)
                ->whereColumn('clicks', '>=', 'expiration_clicks');
        });
    }

    public function scopeDisabled(Builder $query): Builder
    {
        return $query->where('disabled', '=', 1);
    }

    public function scopeActive(Builder $query): Builder
    {
        return $query->where('disabled', '=', 0)
            ->where(function ($query) {
                $query->where('ends_at', '>', Carbon::now())
                    ->orWhere('ends_at', '=', null);
            })
            ->where(function ($query) {
                $query->where(function ($query) {
                    $query->where('expiration_clicks', '=', null)
                        ->orWhere('expiration_clicks', '=', 0);
                })
                ->orWhere(function ($query) {
                    $query->where('expiration_clicks', '>', 0)
                        ->whereColumn('clicks', '<', 'expiration_clicks');
                });
            });
    }

    protected function password(): Attribute
    {
        return new Attribute(
            get: function ($value) {
                try {
                    return Crypt::decryptString($value);
                } catch (\Exception $e) {
                    return null;
                }
            },
            set: fn ($value) => Crypt::encryptString($value),
        );
    }

    protected function privacyPassword(): Attribute
    {
        return new Attribute(
            get: function ($value) {
                try {
                    return Crypt::decryptString($value);
                } catch (\Exception $e) {
                    return null;
                }
            },
            set: fn ($value) => Crypt::encryptString($value),
        );
    }
}
