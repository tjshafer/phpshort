<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Plan
 *
 * @mixin Builder
 */
class Plan extends Model
{
    use SoftDeletes;

    protected $casts = [
        'items' => 'object',
        'tax_rates' => 'object',
        'coupons' => 'object',
        'features' => 'object',
    ];

    /**
     * @param $value
     */
    public function scopeSearchName(Builder $query, $value): Builder
    {
        return $query->where('name', 'like', '%'.$value.'%');
    }

    /**
     * @param $value
     */
    public function scopeOfVisibility(Builder $query, $value): Builder
    {
        return $query->where('visibility', '=', $value);
    }

    public function scopePriced(Builder $query): Builder
    {
        return $query->where([['amount_month', '>', 0], ['amount_year', '>', 0]]);
    }

    public function scopeDefault(Builder $query): Builder
    {
        return $query->where([['amount_month', '=', 0], ['amount_year', '=', 0]]);
    }

    /**
     * Get the plan price status
     */
    public function hasPrice(): bool
    {
        return $this->amount_month || $this->amount_year;
    }
}
