<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Coupon extends Model
{
    use SoftDeletes;

    /**
     * @param $value
     */
    public function scopeSearchName(Builder $query, $value): Builder
    {
        return $query->where('name', 'like', '%'.$value.'%');
    }

    /**
     * @param $value
     */
    public function scopeSearchCode(Builder $query, $value): Builder
    {
        return $query->where('code', 'like', '%'.$value.'%');
    }

    /**
     * @param $value
     */
    public function scopeOfType(Builder $query, $value): Builder
    {
        return $query->where('type', '=', $value);
    }
}
