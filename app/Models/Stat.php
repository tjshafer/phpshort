<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Stat extends Model
{
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'date' => 'datetime:Y-m-d',
    ];

    /**
     * @param $value
     */
    public function scopeSearchValue(Builder $query, $value): Builder
    {
        return $query->where('value', 'like', '%'.$value.'%');
    }
}
