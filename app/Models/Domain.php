<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Domain
 *
 * @mixin Builder
 */
class Domain extends Model
{
    /**
     * @param $value
     */
    public function scopeSearchName(Builder $query, $value): Builder
    {
        return $query->where('name', 'like', '%'.$value.'%');
    }

    /**
     * @param $value
     */
    public function scopeOfUser(Builder $query, $value): Builder
    {
        return $query->where('user_id', '=', $value);
    }

    public function scopeGlobal(Builder $query): Builder
    {
        return $query->where('user_id', '=', 0);
    }

    public function scopePrivate(Builder $query): Builder
    {
        return $query->where('user_id', '=', 1);
    }

    protected function totalLinks(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $this->hasMany(\App\Models\Link::class)->where('domain_id', $this->id)->count(),
        );
    }

    /**
     * Get the Links under the Domain.
     */
    public function links(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(\App\Models\Link::class)->where('domain_id', $this->id);
    }

    /**
     * Get the user that owns the Domain.
     *
     * @return mixed
     */
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class)->withTrashed();
    }

    protected function url(): Attribute
    {
        return new Attribute(
            get: fn ($value) => config('settings.short_protocol').'://'.$this->name,
        );
    }
}
