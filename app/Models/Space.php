<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Space
 *
 * @mixin Builder
 */
class Space extends Model
{
    /**
     * @param $value
     */
    public function scopeSearchName(Builder $query, $value): Builder
    {
        return $query->where('name', 'like', '%'.$value.'%');
    }

    /**
     * @param $value
     */
    public function scopeOfUser(Builder $query, $value): Builder
    {
        return $query->where('user_id', '=', $value);
    }

    protected function totalLinks(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $this->hasMany(\App\Models\Link::class)->where('space_id', $this->id)->count(),
        );
    }

    /**
     * Get the Links under the Space.
     */
    public function links(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(\App\Models\Link::class)->where('space_id', $this->id);
    }

    /**
     * Get the user that owns the Space.
     *
     * @return mixed
     */
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class)->withTrashed();
    }
}
