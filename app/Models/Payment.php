<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'product' => 'object',
        'tax_rates' => 'object',
        'coupon' => 'object',
        'customer' => 'object',
        'seller' => 'object',
    ];

    /**
     * Get the user that owns the payment.
     */
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class)->withTrashed();
    }

    /**
     * Get the plan of the payment.
     */
    public function plan()
    {
        return $this->belongsTo(\App\Models\Plan::class)->withTrashed();
    }

    /**
     * @param $value
     */
    public function scopeSearchPayment(Builder $query, $value): Builder
    {
        return $query->where('payment_id', 'like', '%'.$value.'%');
    }

    /**
     * @param $value
     */
    public function scopeSearchInvoice(Builder $query, $value): Builder
    {
        return $query->where([['invoice_id', 'like', '%'.$value.'%'], ['status', '=', 'completed']]);
    }

    /**
     * @param $value
     */
    public function scopeOfPlan(Builder $query, $value): Builder
    {
        return $query->where('plan_id', '=', $value);
    }

    /**
     * @param $value
     */
    public function scopeOfUser(Builder $query, $value): Builder
    {
        return $query->where('user_id', '=', $value);
    }

    /**
     * @param $value
     */
    public function scopeOfInterval(Builder $query, $value): Builder
    {
        return $query->where('interval', '=', $value);
    }

    /**
     * @param $value
     */
    public function scopeOfProcessor(Builder $query, $value): Builder
    {
        return $query->where('processor', '=', $value);
    }

    /**
     * @param $value
     */
    public function scopeOfStatus(Builder $query, $value): Builder
    {
        return $query->where('status', '=', $value);
    }
}
