<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Domain
 *
 * @mixin Builder
 */
class Pixel extends Model
{
    /**
     * @param $value
     */
    public function scopeSearchName(Builder $query, $value): Builder
    {
        return $query->where('name', 'like', '%'.$value.'%');
    }

    /**
     * @param $value
     */
    public function scopeOfType(Builder $query, $value): Builder
    {
        return $query->where('type', '=', $value);
    }

    /**
     * @param $value
     */
    public function scopeOfUser(Builder $query, $value): Builder
    {
        return $query->where('user_id', '=', $value);
    }

    protected function totalLinks(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $this->belongsToMany(\App\Models\Link::class)->count(),
        );
    }

    /**
     * Get the user that owns the Pixel.
     *
     * @return mixed
     */
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class)->withTrashed();
    }

    /**
     * Get the Links under the Pixel.
     */
    public function links(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(\App\Models\Link::class);
    }
}
