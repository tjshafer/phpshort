<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class ValidateExternalDomainNameRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(
        /**
         * @var
         */
        public $domain
    ) {
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     */
    public function passes($attribute, $value): bool
    {
        $domain = parse_url($this->domain)['host'] ?? $this->domain;

        // If the domain is the same with the installation URL
        if ($domain == parse_url(config('app.url'))['host']) {
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('The :attribute is invalid.');
    }
}
