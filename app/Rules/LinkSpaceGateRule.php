<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class LinkSpaceGateRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @param $user
     * @return void
     */
    public function __construct(
        /**
         * @var
         */
        private $user
    ) {
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     */
    public function passes($attribute, $value): bool
    {
        if ($this->user->can('spaces', [\App\Models\Link::class, $this->user->plan->features->spaces])) {
            return true;
        }

        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('You don\'t have access to this feature.');
    }
}
