<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class ValidateLinkStatsPasswordRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @param $link
     * @return void
     */
    public function __construct(
        /**
         * @var
         */
        private $link
    ) {
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     */
    public function passes($attribute, $value): bool
    {
        if ($this->link->privacy_password == $value) {
            return true;
        }

        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('The entered password is not correct.');
    }
}
