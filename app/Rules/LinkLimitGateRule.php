<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class LinkLimitGateRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @param $user
     * @return void
     */
    public function __construct(
        /**
         * @var
         */
        private $user
    ) {
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if ($this->user->can('create', [\App\Models\Link::class, $this->user->plan->features->links])) {
            return true;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('You shortened too many links.');
    }
}
