<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class LinkDomainGateRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @param $user
     * @return void
     */
    public function __construct(
        /**
         * @var
         */
        private $user
    ) {
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     */
    public function passes($attribute, $value): bool
    {
        if ($this->user->can('domains', [\App\Models\Link::class, $this->user->plan->features->domains]) || $this->user->can('globalDomains', [\App\Models\Link::class, $this->user->plan->features->global_domains]) || (config('settings.short_domain') && config('settings.short_domain') == $value)) {
            return true;
        }

        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('You don\'t have access to this feature.');
    }
}
