<?php

namespace App\Rules;

use App\Models\Space;
use Illuminate\Contracts\Validation\Rule;

class ValidateSpaceOwnershipRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @param $userId
     * @return void
     */
    public function __construct(
        /**
         * @var
         */
        private $userId
    ) {
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     */
    public function passes($attribute, $value): bool
    {
        if (empty($value)) {
            return true;
        }

        if (Space::where([['id', '=', $value], ['user_id', '=', $this->userId]])->exists()) {
            return true;
        }

        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('Invalid value.');
    }
}
