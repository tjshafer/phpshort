<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class FieldNotPresentRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     */
    public function passes($attribute, $value): bool
    {
        return false;
    }

    /**
     * Get the validation error message.
     */
    public function message(): string
    {
        return 'The validation error message.';
    }
}
