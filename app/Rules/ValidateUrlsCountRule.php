<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class ValidateUrlsCountRule implements Rule
{
    /**
     * The input attribute
     *
     * @var
     */
    private $attribute;

    /**
     * Create a new rule instance.
     *
     * @param  int  $count
     */
    public function __construct(
        /**
         * @var
         */
        protected $count = 10
    ) {
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     */
    public function passes($attribute, $value): bool
    {
        $this->attribute = $attribute;

        if (count(preg_split('/\n|\r/', $value, -1, PREG_SPLIT_NO_EMPTY)) > $this->count) {
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('The number of :attribute must not exceed :value.', ['attribute' => $this->attribute, 'count' => $this->count]);
    }
}
