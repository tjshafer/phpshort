<?php

namespace App\Rules;

use App\Models\Link;
use Illuminate\Contracts\Validation\Rule;

class ValidateLinkPasswordRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @param $link
     * @return void
     */
    public function __construct(
        /**
         * The link id
         *
         * @var
         */
        private $link
    ) {
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     */
    public function passes($attribute, $value): bool
    {
        $link = Link::where('id', '=', $this->link)->first();

        if ($value == $link->password) {
            return true;
        }

        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('The entered password is not correct.');
    }
}
