<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        $this->app->singleton(\App\Http\View\Composers\UserStatsComposer::class);
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        View::composer('shared.footer', \App\Http\View\Composers\FooterPagesComposer::class);

        View::composer([
            'dashboard.index',
            'shared.header',
        ], \App\Http\View\Composers\UserStatsComposer::class);
    }
}
