<?php

use App\Http\Controllers\API;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->middleware('auth:api', 'throttle:120')->group(function () {
    Route::apiResource('links', API\LinkController::class, ['parameters' => [
        'links' => 'id',
    ], 'as' => 'api'])->middleware('api.guard');

    Route::apiResource('domains', API\DomainController::class, ['parameters' => [
        'domains' => 'id',
    ], 'as' => 'api'])->middleware('api.guard');

    Route::apiResource('spaces', API\SpaceController::class, ['parameters' => [
        'spaces' => 'id',
    ], 'as' => 'api'])->middleware('api.guard');

    Route::apiResource('pixels', API\PixelController::class, ['parameters' => [
        'pixels' => 'id',
    ], 'as' => 'api'])->middleware('api.guard');

    Route::apiResource('stats', API\StatController::class, ['parameters' => [
        'stats' => 'id',
    ], 'only' => ['show'], 'as' => 'api']);

    Route::apiResource('account', API\AccountController::class, ['only' => [
        'index',
    ], 'as' => 'api'])->middleware('api.guard');

    Route::fallback(fn () => response()->json(['message' => __('Resource not found.'), 'status' => 404], 404));
});
